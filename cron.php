<?php
ini_set("max_execution_time",60000);
ini_set('memory_limit', '1024M');

/**
 * A pseudo-CRON daemon for scheduling WordPress tasks
 *
 * WP Cron is triggered when the site receives a visit. In the scenario
 * where a site may not receive enough visits to execute scheduled tasks
 * in a timely manner, this file can be called directly or via a server
 * CRON daemon for X number of times.
 *
 * Defining DISABLE_WP_CRON as true and calling this file directly are
 * mutually exclusive and the latter does not rely on the former to work.
 *
 * The HTTP request to this file will not slow down the visitor who happens to
 * visit when the cron job is needed to run.
 *
 * @package WordPress
 */

// ignore_user_abort(true);

// if ( !empty($_POST) || defined('DOING_AJAX') || defined('DOING_CRON') )
// 	die();

/**
 * Tell WordPress we are doing the CRON task.
 *
 * @var bool
 */
// define('DOING_CRON', true);

if ( !defined('ABSPATH') ) {
	/** Set up WordPress environment */
	require_once( dirname( __FILE__ ) . '/wp-load.php' );
}

 global $wpdb;
 global $current_user;     
////////////////////////////////////////


  $article_source_table = $wpdb->prefix . "article_source_tbl";
  $lic_key_tbl = $wpdb->prefix . "lic_key_tbl";
  $setting_tbl = $wpdb->prefix . "setting_tbl";
  $flag = 0;
error_reporting(E_ALL);
ini_set('display_errors', 1);
  $result_source = $wpdb->get_results("SELECT * FROM $article_source_table ORDER BY id DESC");

 for($i=0;$i<count($result_source);$i++)
   {
    
      if($result_source[$i]->source_type == "NEWS")
      {         
       
      $result_setting = $wpdb->get_results("SELECT * FROM $setting_tbl ORDER BY id DESC");
      $lang_trans = $result_setting[0]->translate_to_global_lang; 
      $spin__text = $result_setting[0]->spin_text; 


      $results = $wpdb->get_results("SELECT license_key FROM $lic_key_tbl");
      $newlicence = $results[0]->license_key;  


      $ch = curl_init();
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     
       curl_setopt($ch, CURLOPT_URL, 'https://newsapi.org/v2/top-headlines?sources='.$result_source[$i]->source_name.'&apiKey='.$newlicence);
  

      $result = curl_exec($ch);
      curl_close($ch);
      $obj = json_decode($result);

 	  
      $term = term_exists($result_source[$i]->source_category, 'category' );
    
        if(count($term) > 0)
         {
           $wpdocs_cat_id = $term['term_id'];
         }
        else{
        $wpdocs_cat = array('cat_name' => $result_source[$i]->source_category, 'category_description' => $result_source[$i]->source_category , 'category_nicename' => $result_source[$i]->source_category, 'category_parent' => '');
        $wpdocs_cat_id = wp_insert_category($wpdocs_cat);
        }

      if(count($obj->articles))
      {
       for($i=0;$i<count($obj->articles);$i++)
       {

    /***************************Title translation******************************/
        $apiKey = $result_setting[0]->proxy_url; 

        if($lang_trans !='disabled' && $apiKey!="")
        {
            $text_title = $obj->articles[$i]->title;
            $url_title = 'https://www.googleapis.com/language/translate/v2?key=' . $apiKey . '&q=' . rawurlencode($text_title) . '&target='.$lang_trans;
            $handle = curl_init($url_title);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($handle);                 
            $responseDecoded = json_decode($response, true);
            curl_close($handle);

            $post__title = $responseDecoded['data']['translations'][0]['translatedText'];
            $post__title = str_replace("'", "", $post__title); 
        }
        else
        {
            $post__title = $obj->articles[$i]->title;
            $post__title = str_replace("'", "", $post__title); 
        }     
      
        $apiKey = $result_setting[0]->proxy_url; 
       
        require_once (dirname(__FILE__) . "/wp-content/plugins/NewsBuilder-DFY/menu/readability.php");

        $url = $obj->articles[$i]->url;
        if (!preg_match('!^https?://!i', $url)) $url = 'http://'.$url;

        $html = file_get_contents($url);
        $page_content = new Readability($html, $url);
        $page_content->init();
        $post__content = $page_content->articleContent->innerHTML;

     /***********spin code start*********************/
   
        if($spin__text =='wikisynonyms')
        {
         require_once(dirname(__FILE__) . "/wp-content/plugins/NewsBuilder-DFY/menu/text-spinner.php");
         $phpTextSpinner = new PhpTextSpinner();    
         $spinContent = $phpTextSpinner->spinContent($post__content);
         $translated = $phpTextSpinner->runTextSpinner($spinContent);
         $post__content = $translated;
        }
        else
        {        
         $post__content = $page_content->articleContent->innerHTML;
        }
    
    /***********spin code end *********************/

      if($lang_trans !='disabled' && $apiKey!="")
        {
            $text = $page_content->articleContent->innerHTML;
            
            $url = 'https://www.googleapis.com/language/translate/v2?key=' . $apiKey . '&q=' . rawurlencode($text) . '&target='.$lang_trans;

            $handle = curl_init($url);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($handle);                 
            $responseDecoded = json_decode($response, true);
            curl_close($handle);
            $post__content = htmlspecialchars_decode($responseDecoded['data']['translations'][0]['translatedText']);  
        }
        else
        {        
            $post__content = $page_content->articleContent->innerHTML;
        }

  
        if($result_setting[0]->read_more !="" && $result_setting[0]->user_agent !="")
         {
            $find__word  = $result_setting[0]->read_more;
            $replace__string = base64_decode($result_setting[0]->user_agent);
            $post__content = str_replace($find__word,$replace__string ,$post__content);
         }

  
         $dublicate_post = $result_setting[0]->duplicate_post; 

         $banned_words = explode(',', $result_setting[0]->banned_words);
         $required_words = explode(',', $result_setting[0]->required_words);


         $flag_badwords = false;
         $flag_requirwords = false;

         $search_for_content = $obj->articles[$i]->title.' '.$obj->articles[$i]->description;
         
         foreach($banned_words as $badwords)
          {
           if(stristr($search_for_content,$badwords))
            {
             $flag_badwords = true;  
            }     
          }

          foreach($required_words as $requirwords)
          {
           if(stristr($search_for_content,$requirwords))
            {
             $flag_requirwords = true;  
            }      
          }
    

         $post_already = $wpdb->get_results("SELECT * FROM $wpdb->posts where post_title='".ltrim($post__title)."'");

         $min_word_title = $result_setting[0]->min_word_title;
         $max_word_title = $result_setting[0]->max_word_title;
         $str__count = str_word_count($post__title);
         

         $min_word_content = $result_setting[0]->min_word_content;
         $max_word_content = $result_setting[0]->max_word_content;
         $str__count_content = str_word_count($post__content);


    if((count($post_already) == 0)  || ($dublicate_post != "on") )
    {
        if($flag_badwords == false || ($result_setting[0]->banned_words == "" ))
        {   
           if($flag_requirwords == true || ($result_setting[0]->required_words == "" )) 
            {
                if(($str__count > $min_word_title)  || ($min_word_title == ""))
                 { // $min_word_title
                    if(($str__count < $max_word_title)  || ($max_word_title == ""))
                     { // $max_word_title
                       if(($str__count_content > $min_word_content)  || ($min_word_content == ""))
                       { // $min_word_title
                          if(($str__count_content < $max_word_content)  || ($max_word_content == ""))
                           { // $max_word_title


        preg_match_all('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $post__content, $image);

        $articles__thumbnail =="";
        $articles__thumbnail = $image['src'][0];


       if(($result_setting[0]->skip_no_img =="on"))
        {

         if($articles__thumbnail !="")
          {
                              $my_post = array(
                              'post_title'    => $post__title,
                              'post_content'  => $post__content,
                              'post_status'   => 'publish',
                              'post_excerpt'   => $obj->articles[$i]->url,
                              'post_author'   => $current_user->ID,         
                              'post_parent' => 0,
                              'post_category' => array( $wpdocs_cat_id ),
                              'post_type'   => $result_source[$i]->post_sel_type
                            
                            );              
                            $postid = wp_insert_post($my_post);


            $sql = "UPDATE $wpdb->posts SET source_post_id = '".$result_source[$i]->id."' WHERE ID = '".$postid."'";
            $wpdb->query($sql);

           

     
            $image_url        = $articles__thumbnail; // Define the image URL here
            $image_name       = 'wp-header-logo.png';
            $upload_dir       = wp_upload_dir(); // Set upload folder
            $image_data       = file_get_contents($image_url); // Get image data
            $unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
            $filename         = basename( $unique_file_name ); // Create image file name

            // Check folder permission and define file location
            if( wp_mkdir_p( $upload_dir['path'] ) ) {
                $file = $upload_dir['path'] . '/' . $filename;
            } else {
                $file = $upload_dir['basedir'] . '/' . $filename;
            }

            // Create the image  file on the server
            file_put_contents( $file, $image_data );

            // Check image file type
            $wp_filetype = wp_check_filetype( $filename, null );

            // Set attachment data
            $attachment = array(
                'post_mime_type' => $wp_filetype['type'],
                'post_title'     => sanitize_file_name( $filename ),
                'post_content'   => '',
                'post_status'    => 'inherit'
            );

                // Create the attachment
                $attach_id = wp_insert_attachment( $attachment, $file, $postid );

                // Include image.php
                require_once(ABSPATH . 'wp-admin/includes/image.php');

                // Define attachment metadata
                $attach_data = wp_generate_attachment_metadata( $attach_id, $file );

                // Assign metadata to attachment
                wp_update_attachment_metadata( $attach_id, $attach_data );

                // And finally assign featured image to post
                set_post_thumbnail( $postid, $attach_id );

              }
          }
          else
          { 

              
            preg_match_all('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $post__content, $image);
            $articles__thumbnail = $image['src'][0];

            if($articles__thumbnail =="")
            {
                $articles__thumbnail = "https://dummyimage.com/600x400/b6c9d4/fff.png&text=No+Image+Available.!";
            }
            else
            {
                $articles__thumbnail = $image['src'][0];
            }



              $my_post = array(
                              'post_title'    => $post__title,
                              'post_content'  => $post__content,
                              'post_status'   => 'publish',
                              'post_excerpt'   => $obj->articles[$i]->url,
                              'post_author'   => $current_user->ID,         
                              'post_parent' => 0,
                              'post_category' => array( $wpdocs_cat_id ),
                              'post_type'   => $result_source[$i]->post_sel_type
                            
                            );              
                            $postid = wp_insert_post($my_post);


            $sql = "UPDATE $wpdb->posts SET source_post_id = '".$result_source[$i]->id."' WHERE ID = '".$postid."'";
            $wpdb->query($sql);

           

     
            $image_url        = $articles__thumbnail; // Define the image URL here
            $image_name       = 'wp-header-logo.png';
            $upload_dir       = wp_upload_dir(); // Set upload folder
            $image_data       = file_get_contents($image_url); // Get image data
            $unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
            $filename         = basename( $unique_file_name ); // Create image file name

            // Check folder permission and define file location
            if( wp_mkdir_p( $upload_dir['path'] ) ) {
                $file = $upload_dir['path'] . '/' . $filename;
            } else {
                $file = $upload_dir['basedir'] . '/' . $filename;
            }

            // Create the image  file on the server
            file_put_contents( $file, $image_data );

            // Check image file type
            $wp_filetype = wp_check_filetype( $filename, null );

            // Set attachment data
            $attachment = array(
                'post_mime_type' => $wp_filetype['type'],
                'post_title'     => sanitize_file_name( $filename ),
                'post_content'   => '',
                'post_status'    => 'inherit'
            );

                // Create the attachment
                $attach_id = wp_insert_attachment( $attachment, $file, $postid );

                // Include image.php
                require_once(ABSPATH . 'wp-admin/includes/image.php');

                // Define attachment metadata
                $attach_data = wp_generate_attachment_metadata( $attach_id, $file );

                // Assign metadata to attachment
                wp_update_attachment_metadata( $attach_id, $attach_data );

                // And finally assign featured image to post
                set_post_thumbnail( $postid, $attach_id );


          }  



                } 




                 } 
                } 
              } 
            } 
         } 
        } 

    } 
      
  } 
     
    } // if close
    else
    {

      $result_setting = $wpdb->get_results("SELECT * FROM $setting_tbl ORDER BY id DESC");
      $lang_trans = $result_setting[0]->translate_to_global_lang; 
      $spin__text = $result_setting[0]->spin_text;
      $dublicate_post = $result_setting[0]->duplicate_post; 

     $content = file_get_contents($result_source[$i]->source_name);
     $x = new SimpleXmlElement($content);


   if(count($x->channel->item))
    {
      for($i=0;$i<count($x->channel->item);$i++)
        {
  
        $post__title = $x->channel->item[$i]->title;
        $post__title = str_replace("'", "", $post__title);

        $post__content = $x->channel->item[$i]->description;
        $articles__url = $x->channel->item[$i]->link;
        
      
        require_once (dirname(__FILE__) . "/wp-content/plugins/NewsBuilder-DFY/menu/readability.php");

        $url = $articles__url;
        if (!preg_match('!^https?://!i', $url)) $url = 'http://'.$url;

        $html = file_get_contents($url);
        $page_content = new Readability($html, $url);
        $page_content->init();
        $post__content = $page_content->articleContent->innerHTML;

        /***********spin code start*********************/
   
        if($spin__text =='wikisynonyms')
        {
         require_once(dirname(__FILE__) . "/text-spinner.php");
         $phpTextSpinner = new PhpTextSpinner();    
         $spinContent = $phpTextSpinner->spinContent($post__content);
         $translated = $phpTextSpinner->runTextSpinner($spinContent);
         $post__content = $translated;
        }
        else
        {        
         $post__content = $page_content->articleContent->innerHTML;
        }
    
    /***********spin code end *********************/

      if($result_setting[0]->read_more !="" && $result_setting[0]->user_agent !="")
           {
              $find__word  = $result_setting[0]->read_more;
              $replace__string = base64_decode($result_setting[0]->user_agent);
              $post__content = str_replace($find__word,$replace__string ,$post__content);
           }


        $post_already = $wpdb->get_results("SELECT * FROM $wpdb->posts where post_title ='".ltrim($post__title)."'");
    

        preg_match_all('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $post__content, $image);
        $articles__thumbnail = $image['src'][0];
      
        $term = term_exists($result_source[$i]->source_category, 'category' );
    
           
        if(count($term) > 0)
        {
           $wpdocs_cat_id = $term['term_id'];
        }
      


        if(($result_setting[0]->skip_no_img =="on"))
        {
         if($articles__thumbnail !="")
          {
           if((count($post_already) == 0)  || ($dublicate_post != "on") )
              {

          $my_post = array(
          'post_title'    => $post__title,
          'post_content'  => $post__content,
          'post_status'   => 'publish',
          'post_excerpt'   => $articles__url,
          'post_author'   => $current_user->ID,         
          'post_parent' => 0,
          'post_category' => array( $wpdocs_cat_id ),
          'post_type'   => $result_source[$i]->post_sel_type
        
        ); 

        $postid = wp_insert_post($my_post);


        $image_url        = $articles__thumbnail; // Define the image URL here
        $image_name       = 'wp-header-logo.png';
        $upload_dir       = wp_upload_dir(); // Set upload folder
        $image_data       = file_get_contents($image_url); // Get image data
        $unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
        $filename         = basename( $unique_file_name ); // Create image file name

        // Check folder permission and define file location
        if( wp_mkdir_p( $upload_dir['path'] ) ) {
            $file = $upload_dir['path'] . '/' . $filename;
        } else {
            $file = $upload_dir['basedir'] . '/' . $filename;
        }

        // Create the image  file on the server
        file_put_contents( $file, $image_data );

        // Check image file type
        $wp_filetype = wp_check_filetype( $filename, null );

        // Set attachment data
        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title'     => sanitize_file_name( $filename ),
            'post_content'   => '',
            'post_status'    => 'inherit'
        );

        // Create the attachment
        $attach_id = wp_insert_attachment( $attachment, $file, $postid );

        // Include image.php
        require_once(ABSPATH . 'wp-admin/includes/image.php');

        // Define attachment metadata
        $attach_data = wp_generate_attachment_metadata( $attach_id, $file );

        // Assign metadata to attachment
        wp_update_attachment_metadata( $attach_id, $attach_data );

        // And finally assign featured image to post
        set_post_thumbnail( $postid, $attach_id );

/***********************end code Featured Image to Post*****************************/
         }

       }
      }
      else
      {

         $dublicate_post = $result_setting[0]->duplicate_post; 

         if($articles__thumbnail =="")
        {
            $articles__thumbnail = "https://dummyimage.com/600x400/b6c9d4/fff.png&text=No+Image+Available.!";
        }
        else
        {
            $articles__thumbnail = $image['src'][0];
        }


       if((count($post_already) == 0)  || ($dublicate_post != "on") )
          {

         $my_post = array(
          'post_title'    => $post__title,
          'post_content'  => $post__content,
          'post_status'   => 'publish',
          'post_excerpt'   => $articles__url,
          'post_author'   => $current_user->ID,         
          'post_parent' => 0,
          'post_category' => array( $wpdocs_cat_id ),
          'post_type'   => $result_source[$i]->post_sel_type
        
        ); 

        $postid = wp_insert_post($my_post);


        $image_url        = $articles__thumbnail; // Define the image URL here
        $image_name       = 'wp-header-logo.png';
        $upload_dir       = wp_upload_dir(); // Set upload folder
        $image_data       = file_get_contents($image_url); // Get image data
        $unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
        $filename         = basename( $unique_file_name ); // Create image file name

        // Check folder permission and define file location
        if( wp_mkdir_p( $upload_dir['path'] ) ) {
            $file = $upload_dir['path'] . '/' . $filename;
        } else {
            $file = $upload_dir['basedir'] . '/' . $filename;
        }

        // Create the image  file on the server
        file_put_contents( $file, $image_data );

        // Check image file type
        $wp_filetype = wp_check_filetype( $filename, null );

        // Set attachment data
        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title'     => sanitize_file_name( $filename ),
            'post_content'   => '',
            'post_status'    => 'inherit'
        );

        // Create the attachment
        $attach_id = wp_insert_attachment( $attachment, $file, $postid );

        // Include image.php
        require_once(ABSPATH . 'wp-admin/includes/image.php');

        // Define attachment metadata
        $attach_data = wp_generate_attachment_metadata( $attach_id, $file );

        // Assign metadata to attachment
        wp_update_attachment_metadata( $attach_id, $attach_data );

        // And finally assign featured image to post
        set_post_thumbnail( $postid, $attach_id );


      } // dublicate post





      }

     } 
   }          

      /// end code  

    }



   } 
   

