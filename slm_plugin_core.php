<?php
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
ini_set('max_execution_time', 3000);
error_reporting(0);

/********************code to get leads from URL *************************************************/
if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
$ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
$ip = $_SERVER['REMOTE_ADDR'];
}

$https = ((!empty($_SERVER['HTTPS'])) && ($_SERVER['HTTPS'] != 'off')) ? true : false;

if($https) {
    $post__url = "https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
} else {
    $post__url = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
}


global $wpdb;
define('SLM_TBL_LICENSE_KEYS', $wpdb->prefix . "lic_key_tbl");
define('SLM_TBL_LIC_DOMAIN', $wpdb->prefix . "article_source_tbl");
define('SLM_TBL_LIC_SETTING', $wpdb->prefix . "setting_tbl");
define('SLM_MANAGEMENT_PERMISSION', 'manage_options');
define('SLM_MAIN_MENU_SLUG', 'slm-main');
define('SLM_MAIN_MENU_SLUG2', 'slm-main2');
define('SLM_MENU_ICON', 'http://automotive.newsbuilder-test.live/plogo.png');

//Includes
include_once('includes/slm-debug-logger.php');
include_once('includes/slm-utility.php');
include_once('includes/slm-init-time-tasks.php');
include_once('includes/slm-api-utility.php');
include_once('includes/slm-api-listener.php');
include_once('includes/slm-third-party-integration.php');
//Include admin side only files
if (is_admin()) {
    include_once('menu/slm-admin-init.php');
    include_once('menu/includes/slm-list-table-class.php'); //Load our own WP List Table class
    include_once('menu/add-news.php');
    include_once('menu/news_rule_class.php');
}

//Action hooks
add_action('init', 'slm_init_handler');
add_action('plugins_loaded', 'slm_plugins_loaded_handler');
add_action('wp_head', 'og_metatags', 4);
add_filter('wp_kses_allowed_html','allow_post_tags', 1);

function allow_post_tags( $allowedposttags ){
    $allowedposttags['script'] = array(
        'type' => true,
        'src' => true,
        'height' => true,
        'width' => true,
    );
    $allowedposttags['iframe'] = array(
        'src' => true,
        'width' => true,
        'height' => true,
        'class' => true,
        'frameborder' => true,
        'webkitAllowFullScreen' => true,
        'mozallowfullscreen' => true,
        'allowFullScreen' => true
    );
    return $allowedposttags;
}
//Initialize debug logger
global $slm_debug_logger;
$slm_debug_logger = new SLM_Debug_Logger();

//Do init time tasks
function slm_init_handler() {
    $init_task = new SLM_Init_Time_Tasks();
    $api_listener = new SLM_API_Listener();
}

//Do plugins loaded time tasks
function slm_plugins_loaded_handler() {
    //Runs when plugins_loaded action gets fired
    if (is_admin()) {
        //Check if db update needed
        if (get_option('wp_lic_mgr_db_version') != WP_LICENSE_MANAGER_DB_VERSION) {
            require_once(dirname(__FILE__) . '/slm_installer.php');
        }
    }

}

//TODO - need to move this to an ajax handler file
add_action('wp_ajax_del_reistered_domain', 'slm_del_reg_dom');
function slm_del_reg_dom() {
    global $wpdb;
    $reg_table = SLM_TBL_LIC_DOMAIN;
    $id = strip_tags($_GET['id']);
    $ret = $wpdb->query("DELETE FROM $reg_table WHERE id='$id'");
    echo ($ret) ? 'success' : 'failed';
    exit(0);
}


$plugin_dir = plugin_dir_path( __FILE__ ) . 'cron.php';
$path = ABSPATH.'/cron.php';

    if (!copy($plugin_dir, $path)) {
        echo "failed to copy $plugin_dir to $path...\n";
    }

// and make sure it's called whenever WordPress loads
add_action('wp', 'cronstarter_activation');

function cronstarter_activation() {
 if( !wp_next_scheduled( 'NewsBuilder' ) ) 
  {  
     wp_schedule_event( time(), 'every_ten_sec', 'NewsBuilder' );   
  }
}


function isa_add_cron_recurrence_interval( $schedules ) {
    
    $schedules['every_ten_sec'] = array(
            'interval'  => 10,
            'display'   => __( 'Every 10 Second', 'textdomain' )
    );  
     
    return $schedules;
}

add_filter( 'cron_schedules', 'isa_add_cron_recurrence_interval' );

// here's the function we'd like to call with our cron job
function my_repeat_function() { 
  
}
 
// hook that function onto our scheduled event:
add_action ('NewsBuilder', 'my_repeat_function');


function og_metatags() {
global $post;
 
}
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
'https://newsbuilder.io/updater/newsbuilder-dfy.json',
plugin_dir_path( __FILE__ ).'slm_bootstrap.php',
'NewsBuilder'
);