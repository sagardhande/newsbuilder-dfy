(`id`, `source_name`, `auth_id`, `schedule_post`, `no_post`, `post_status`, `post_sel_type`, `date_created`, `source_type`, `source_category`) VALUES
(1, 'https://www.elle.com/rss/all.xml/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Latest News'),
(2, 'https://www.whowhatwear.com/rss', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Latest News'),
(3, 'https://fashionmagazine.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Latest News'),
(4, 'https://www.gq.com/feed/style/rss', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Latest News'),
(5, 'https://www.theblondesalad.com/en-IN/feed', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Latest News'),
(6, 'https://www.refinery29.com/fashion/rss.xml', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Latest News'),
(7, 'https://www.popsugar.com/fashion/feed', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Latest News'),
(8, 'https://www.yesstyle.com/blog/feed/atom/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Latest News'),
(9, 'https://www.fashionlady.in/feed', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Latest News'),
(10, 'https://thecurvyfashionista.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Fashion'),
(11, 'https://www.fashionbeans.com/rss-feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Fashion'),
(12, 'http://fashionbombdaily.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Fashion'),
(13, 'http://www.smudgetikka.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Kids Fashion'),
(14, 'https://juniorstyle.net/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Kids Fashion'),
(15, 'http://posterchildmag.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Kids Fashion'),
(16, 'https://twindollicious.blog/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Kids Fashion'),
(17, 'http://feeds.feedburner.com/HarperBeckhamFashionBlog', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Kids Fashion'),
(18, 'https://childrensretailtoday.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Kids Fashion'),
(19, 'http://www.thirdeyechicfashion.com/feeds/posts/default?alt=rss', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Kids Fashion'),
(20, 'http://blog.fabkids.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Kids Fashion'),
(21, 'https://strollerinthecity.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Kids Fashion'),
(22, 'https://www.southindiafashion.com/category/Kids-Fashion/feed', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Kids Fashion'),
(23, 'http://www.pauletpaula.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Kids Fashion'),
(24, 'https://blog.sophiasstyle.com/feed/?x=1', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Kids Fashion'),
(25, 'http://blog.fabkids.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Kids Fashion'),
(26, 'https://makeuptutorials.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Music'),
(27, 'https://makeuptutorials.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Makeup'),
(28, 'https://www.makeupandbeautyblog.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Makeup'),
(29, 'http://www.lisaeldridge.com/rss/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Makeup'),
(30, 'http://hudabeauty.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Makeup'),
(31, 'https://makeupandbeauty.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Makeup'),
(32, 'https://www.reddit.com/r/MakeupAddiction/.rss?format=xml', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Makeup'),
(33, 'http://corallista.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Makeup'),
(34, 'https://janeiredale.com/us/en/makeup-blog/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Makeup'),
(35, 'https://www.gloskinbeauty.com/blog/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Makeup'),
(36, 'http://www.beautifulhameshablog.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Makeup'),
(37, 'http://www.thesmallthingsblog.com/category/makeup/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Makeup'),
(38, 'https://mybeautybunny.com/category/makeup/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Makeup'),
(39, 'https://realtechniques.com/blog/feed', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Makeup'),
(40, 'https://www.fashionbeans.com/rss-feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Mens Fashion'),
(41, 'https://www.fashionbeans.com/rss-feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Mens Fashion'),
(42, 'https://www.mainlinemenswear.co.uk/blog/feed', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Mens Fashion'),
(43, 'https://www.menstylefashion.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Mens Fashion'),
(44, 'https://www.dmarge.com/feed', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Mens Fashion'),
(45, 'https://www.gentlemansgazette.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Mens Fashion'),
(46, 'https://www.thefashionisto.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Mens Fashion'),
(47, 'http://www.mensfashionmagazine.com/feed', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Mens Fashion'),
(48, 'https://www.menswearstyle.co.uk/rss', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Mens Fashion'),
(49, 'https://menstyle1.com/rss#_=_', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Mens Fashion'),
(50, 'https://www.reddit.com/r/malefashion/.rss?format=xml', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Mens Fashion'),
(51, 'https://theidleman.com/manual/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Mens Fashion'),
(52, 'http://feeds.feedburner.com/RealMenRealStyle', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Mens Fashion'),
(53, 'https://www.temptalia.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Beauty'),
(54, 'http://feeds.feedburner.com/IndianMakeupAndBeautyBlog', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Beauty'),
(55, 'https://mybeautybunny.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Beauty'),
(56, 'http://feeds.feedblitz.com/gouldyloxreviews', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Beauty'),
(57, 'http://feeds.feedburner.com/intothegloss/VUQZ', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Beauty'),
(58, 'http://www.bbeautilicious.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Beauty'),
(59, 'http://feeds.feedburner.com/PrimeBeautyBlog', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Beauty'),
(60, 'https://britishbeautyblogger.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Beauty'),
(61, 'https://beautybulletins.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Beauty'),
(62, 'https://www.gloskinbeauty.com/blog/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Beauty'),
(63, 'https://www.latestinbeauty.com/blog/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Beauty'),
(64, 'http://www.heyaprill.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Beauty'),
(65, 'https://thebeautifultruth.ie/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Beauty'),
(66, 'https://www.makeupandbeautyblog.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Beauty'),
(67, 'https://dappered.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-30', 'RSS', 'Mens Fashion');