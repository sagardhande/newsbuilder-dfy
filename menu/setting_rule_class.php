<?php

class WPLM_List_Setting extends WP_License_Mgr_List_Table {
    
    function __construct(){
        global $status, $page,$wpdb;
                
        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'item',     //singular name of the listed records
            'plural'    => 'items',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );
        
    }


 function prepare_setting_list() {


if (isset($_POST['btnSubmit']) ) { 

    global $wpdb;
    global $current_user;
    extract($_POST);
    $setting_tbl = $wpdb->prefix . "setting_tbl";
   


    $duplicatepost = $_POST['duplicate_post'];
    $rulesettingpost = $_POST['rule_setting_post'];
    $metaboxpost = $_POST['metabox_post'];
    $loggingrule = $_POST['logging_rule'];
    $detailloggingrule = $_POST['detail_logging_rule'];
    $autoclearlog = $_POST['auto_clear_log'];
    $useragent = base64_encode($_POST['user_agent']);
    $proxy_url = $_POST['proxy_url'];
    $proxy_auth = $_POST['proxy_auth'];
    $timeout_rule_run = $_POST['timeout_rule_run'];
    $send_rule_run = $_POST['send_rule_run']; 
    $original_post_content = $_POST['original_post_content'];
    $skip_post_import_html = $_POST['skip_post_import_html'];
    $skip_post_extract = $_POST['skip_post_extract'];
    $generate_inexist_categories = $_POST['generate_inexist_categories'];
    $generate_post_content = $_POST['generate_post_content'];
    $htmltag_generate_post = $_POST['htmltag_generate_post'];
    $js_crawler_content = $_POST['js_crawler_content'];
    $html_final_content_id = $_POST['html_final_content_id'];
    $html_final_content_class = $_POST['html_final_content_class'];
    $read_more = $_POST['read_more'];
    $auto_translate_to_global = $_POST['auto_translate_to_global'];
    $translate_to_global_lang = $_POST['translate_to_global_lang'];
    $keep_link_source = $_POST['keep_link_source'];
    $spin_text = $_POST['spin_text'];
    $no_spin_title = $_POST['no_spin_title']; 
    $min_word_title = $_POST['min_word_title'];
    $max_word_title = $_POST['max_word_title'];
    $min_word_content = $_POST['min_word_content'];
    $max_word_content = $_POST['max_word_content'];
    $banned_words = $_POST['banned_words'];
    $required_words = $_POST['required_words'];
    $required_all = $_POST['required_all'];
    $skip_no_img = $_POST['skip_no_img'];
    $skip_old = $_POST['skip_old'];
    $no_local_image = $_POST['no_local_image'];
    $resize_width = $_POST['resize_width'];
    $resize_height = $_POST['resize_height'];

  $sql2 = "Delete from $setting_tbl";  
  $wpdb->query($sql2);

  $sql = "INSERT INTO $setting_tbl (auth_id,duplicate_post, rule_setting_post, metabox_post,logging_rule,
                                        detail_logging_rule,auto_clear_log,user_agent,proxy_url,proxy_auth,
                                        timeout_rule_run,send_rule_run,original_post_content,skip_post_import_html,skip_post_extract,generate_inexist_categories,generate_post_content,htmltag_generate_post,
                                        js_crawler_content,html_final_content_id,html_final_content_class,
                                        read_more,auto_translate_to_global,translate_to_global_lang,
                                        keep_link_source,spin_text,no_spin_title,min_word_title,
                                        max_word_title,min_word_content,max_word_content,banned_words,
                                        required_words,required_all,skip_no_img,skip_old,no_local_image,
                                        resize_width,resize_height,date_created) 
                                        VALUES 
                                         ('$current_user->ID','$duplicatepost', '$rulesettingpost', '$metaboxpost','$loggingrule','$detailloggingrule','$autoclearlog','$useragent','$proxy_url','$proxy_auth','$timeout_rule_run','$send_rule_run','$original_post_content','$skip_post_import_html','$skip_post_extract','$generate_inexist_categories','$generate_post_content','$htmltag_generate_post','$js_crawler_content','$html_final_content_id','$html_final_content_class','$read_more','$auto_translate_to_global','$translate_to_global_lang','$keep_link_source','$spin_text','$no_spin_title','$min_word_title','$max_word_title','$min_word_content','$max_word_content','$banned_words','$required_words','$required_all','$skip_no_img','$skip_old','$no_local_image','$resize_width','$resize_height', CURRENT_TIMESTAMP)";


    $wpdb->query($sql);
  
  $insertid = $wpdb->insert_id;
    

   echo '<script type="text/javascript">location.reload(true);</script>';

       
        }
            
       
    }
}