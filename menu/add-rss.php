<?php
function add_rss_function() {
    
    global $wpdb;
    echo '<div class="wrap">';
    echo '<h2>Add New Article</h2>';
    echo '<div id="poststuff"><div id="post-body">';
    ?>

    <div class="postbox">

    <form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">

      <h3 class="hndle"><label for="title">Add New Feed Source </label></h3>

    <table width="100%" >
      <tr>
        <td width="35%">
          <div class="inside">
            <strong>Custom Feed Source URL</strong>
            <br />               
              <input type="text"  name="feed_source_url" style="width:350px;">                  
              </div>
        </td>
<?php 
   $categories = get_categories();
?>
          <td width="30%" >
            <div class="inside">
            <strong>Category</strong>
            <br />               
              <select id="date" name="news_rules_list_category" style="width:100%;">
                   <option disabled="" >--Default Category--</option>
                    <?php
                      foreach($categories as $category) {
                    ?>
                    <option value="<?php echo $category->name;?>">
                    <?php echo $category->name;?>
                    </option>
                    <?php
                    }
                    ?>
                    <option disabled="" >--New Category--</option>
                    <option selected="selected" value="Uncategorized">Uncategorized</option>
                    <option value="Business"> Business</option>
                    <option value="Entertainment"> Entertainment</option> 
                    <option value="Gaming"> Gaming</option> 
                    <option value="General"> General</option> 
                    <option value="Health-and-Medical"> Health and Medical</option> 
                    <option value="Music"> Music</option> 
                    <option value="Politics"> Politics</option> 
                    <option value="Science-and-Nature"> Science and Nature</option> 
                    <option value="Sport"> Sport</option> 
                    <option value="Technology"> Technology</option>
                    
                  
              </select>          
            </div>
        </td>

         
        <td width="9%">
            <div class="inside">
             <strong>Max # Posts </strong>
            <br />               
             <input type="number" step="1" min="0" placeholder="# max" max="100" name="no_post" style="width:100px;" value="10" required="">       
            </div>
        </td>

        <td width="9%">
            <div class="inside">
             <strong>Post Status </strong>
            <br />               
            <select id="post_status" name="post_status" style="width:170px;">
              <option value="pending">Moderate -&gt; Pending</option>
              <option value="draft">Moderate -&gt; Draft</option>
              <option value="publish" selected="">Published</option>
              <option value="private">Private</option>
              <option value="trash">Trash</option>
            </select>    
            </div>
         </td>
           
         <td width="9%">
           <div class="inside">
             <strong>Item Type </strong>
            <br />               
            <select id="post_sel_type" name="post_sel_type" style="width:170px;">
             <option value="post" selected="">post</option>
             <option value="page">page</option>
             <option value="attachment">attachment</option>
             <option value="revision">revision</option>
             <option value="nav_menu_item">nav_menu_item</option>
            
            </select>    
           </div>
        </td>
         
      
        <td width="9%">
            <div class="inside">
             
            <br />               
            <input type="submit" name="save_rss_feed" class="button button-primary" value="Save RSS Feed" />
            <br />            
            </div>
        </td>
      
      </tr>

      <tr><td width="50%" colspan="6">
          
         <div class="inside">
             <strong>Default Feed Source URL </strong>
            <br />               
            <select id="default_feed_source_url" name="default_feed_source_url" style="width:350px;">
                <option value="">Please Select</option>   
                <option disabled >------------------------Game------------------------</option>   
                <option value="http://feeds.feedburner.com/GamasutraNews">Gamasutra News</option>
                <option value="https://www.techradar.com/rss/reviews/gaming">Techradar</option>
                <option value="http://feeds.ign.com/ign/games-all">IGN</option>
                <option value="https://www.gamespot.com/feeds/game-news/">Gamespot</option>              
                <option value="https://n4g.com/rss/news?channel=&sort=latest">N4G Latest </option>
                <option value="https://n4g.com/rss/news?channel=ps4&sort=latest">N4G PS4</option>
                <option value="https://n4g.com/rss/news?channel=ps3&sort=latest">N4G PS3</option>
                <option value="https://n4g.com/rss/news?channel=xbox-one&sort=latest">N4G XBOX One</option>
                <option value="https://n4g.com/rss/news?channel=xbox-360&sort=latest">N4G XBOX 360</option>
                <option value="https://n4g.com/rss/news?channel=xbox&sort=latest">N4G XBOX</option>
                <option value="https://n4g.com/rss/news?channel=ps2&sort=latest">N4G PS2</option>
                <option value="https://news.xbox.com/en-us/feed/">XBOX</option>
                <option value="http://feeds.feedburner.com/psblog">PS Blog</option>
                <option value="http://www.nintendolife.com/feeds/latest">Nintendo Life</option>
                <option value="https://www.gameinformer.com/rss.xml">Game Informer</option>
                <option value="https://www.reddit.com/r/gamers/.rss">Reddit Gamers</option>
                <option value="https://www.pcgamesn.com/rss">PCGamesN</option>
                <option value="https://kotaku.com/rss">Kotaku</option>
                <option value="https://www.techradar.com/rss/reviews/gaming">TechRadar - Gaming reviews</option>    
                <option value="http://feeds.ign.com/IGNXboxOneReviews">IGNXboxOneReviews</option>
                 <option value="http://feeds.ign.com/ign/xbox-360-reviews">IGNXbox 360 Reviews</option>
                <option value="http://feeds.ign.com/IGNPS4Reviews">IGNPS4Reviews</option>        
                <option value="https://n4g.com/rss/news?channel=android&sort=latest">N4G Android</option>
                <option value="https://n4g.com/rss/news?channel=mobile&sort=latest">N4G Mobile</option>
                <option value="https://n4g.com/rss/news?channel=iphone&sort=latest">N4G iphone</option>
                <option value="https://n4g.com/rss/news?channel=ipad&sort=latest">N4G ipad</option>
                <option value="http://feeds.ign.com/ign/pc-all">IGN PC All</option>
                <option value="http://feeds.ign.com/ign/pc-articles">IGN PC Articles</option>
                <option value="http://feeds.ign.com/ign/wireless-all">IGN Ireless All</option>
                <option value="http://feeds.ign.com/ign/wireless-articles">IGN Wireless Articles</option>
                <option value="https://www.techradar.com/rss/news/gaming">IGN Gaming</option>
                <option value="http://feeds.ign.com/ign/articles">IGN Articles</option>
                <option value="https://n4g.com/rss/news?channel=&sort=latest">IGN Latest</option>
                <option value="http://dualshockers.com/feed/atom">IGN Atom</option>
                <option value="http://feeds.ign.com/ign/wii-u-articles">IGN WII Articles</option>
                <option disabled >----------------------Technology----------------------</option> 
                <option value="https://www.techmeme.com/feed.xml">Techmeme</option>
                <option value="http://feeds.feedburner.com/TechCrunch">TechCrunch</option>
                <option value="https://www.technologyreview.com/topnews.rss">Technology Review</option>
                <option value="http://feeds.arstechnica.com/arstechnica/technology-lab">Arstechnica Technology-Lab</option>
                <option value="https://readwrite.com/feed/?x=1">ReadWrite</option>
                <option value="https://www.recode.net/rss/index.xml">Recode</option>
                <option value="http://blogs.barrons.com/techtraderdaily/feed/">Barrons</option>
                <option value="http://feeds.feedblitz.com/freetech4teachers">Feed Blitz</option>
                <option value="https://www.computerworld.com/index.rss">ComputerWorld</option>
                <option value="https://www.computerweekly.com/rss/RSS-Feed.xml">ComputerWeekly</option>
                <option value="https://www.computerweekly.com/rss/Latest-IT-news.xml">ComputerWeekly Latest-IT-News</option>
                <option value="https://www.computerweekly.com/rss/IT-management.xml">ComputerWeekly IT-Management</option>
                <option value="https://www.computerweekly.com/rss/IT-careers-and-IT-skills.xml">ComputerWeekly IT-Careers</option>
                <option value="https://www.computerweekly.com/rss/IT-services-and-outsourcing.xml">ComputerWeekly IT-Services</option>
                <option value="https://www.computerweekly.com/rss/IT-security.xml">ComputerWeekly IT-security</option>
                <option value="https://www.computerweekly.com/rss/Networking-and-communication.xml">ComputerWeekly /Networking & Communication</option>
                <option value="https://www.computerweekly.com/rss/Mobile-technology.xml">ComputerWeekly Mobile-Technology</option>
                <option value="https://www.computerweekly.com/rss/Internet-technology.xml">ComputerWeekly</option>
                <option value="https://www.computerweekly.com/rss/IT-hardware.xml">ComputerWeekly IT-Hardware</option>
                <option value="https://feeds.pcmag.com/Rss.aspx/SectionArticles?sectionId=1475">PCMag.com</option>
                <option value="http://www.informationweek.com/rss_simple.asp">Information Week</option>
                <option value="https://www.infoworld.com/index.rss">Info World</option>
                <option value="https://www.computerweekly.com/rss/All-Computer-Weekly-content.xml">ComputerWeekly All-Computer</option>
                <option value="https://www.techrepublic.com/rssfeeds/topic/artificial-intelligence/">TechRepublic Artificial Intelligence</option>
                <option value="https://www.techrepublic.com/rssfeeds/topic/big-data/">TechRepublic Big Data</option>
                <option value="https://www.techrepublic.com/rssfeeds/topic/cloud/">TechRepublic Cloud</option>
                <option value="https://www.techrepublic.com/rssfeeds/topic/data-centers/">TechRepublic Data-Centers</option>
                <option value="https://www.techrepublic.com/rssfeeds/topic/developer/">TechRepublic Developer</option>
                <option value="https://www.techrepublic.com/rssfeeds/topic/e-commerce/">TechRepublic e-commerce</option>
                <option value="https://www.techrepublic.com/rssfeeds/topic/gdpr/">TechRepublic GDPR</option>
                <option value="https://www.techrepublic.com/rssfeeds/topic/google/">TechRepublic Google</option>
                <option value="https://www.techrepublic.com/rssfeeds/topic/innovation/">TechRepublic Innovation</option>
                <option value="https://www.techrepublic.com/rssfeeds/topic/internet-of-things/">TechRepublic Internet-Of-Things</option>
                <option value="https://www.techrepublic.com/rssfeeds/topic/security/">TechRepublic security</option>
                <option value="https://galido.net/blog/feed/">Galido</option>
                <option value="https://go.forrester.com/blogs/feed/">Forrester</option>
                <option value="https://www.zdnet.com/news/rss.xml">Zdnet</option>
                <option value="https://blogs.absolute.com/feed/">Absolute</option>
                <option value="https://www.gsmarena.com/rss-news-reviews.php3">Gsmarena</option>
                <option value="https://pocketnow.com/feed/">Pocket Now</option>
                <option value="https://www.fairphone.com/en/blog/feed/">Fairphone</option>
                <option value="https://www.androidcentral.com/feed">Androidcentral</option>
                <option value="https://www.gizmochina.com/category/news/feed/">Gizmochina</option>
                <option value="https://news.google.com/news?cf=all&hl=en&pz=1&ned=us&q=smartphone&output=rss">Google News Smartphone </option>
                <option value="https://www.reddit.com/r/smartphone/.rss">Reddit Smartphone</option>
                <option value="http://feeds.feedburner.com/coolsmartphone/uJxV">Feedburner Cool Smart Phone</option>
                <option value="https://www.androidpit.com/feed/main.xml">Android Pit</option>
                <option value="https://www.gizchina.com/feed/">Gizchina</option>
                <option value="https://blog.feedspot.com/mobile_rss_feeds/">Feedspot Mobile</option>
                <option value="https://www.engadget.com/rss.xml">Engadget</option>
                <option value="https://www.cnet.com/rss/news/">Cnet</option>
                <option value="https://gizmodo.com/rss">Gizmodo</option>
                <option value="https://feeds.feedburner.com/ubergizmo">Feedburner Ubergizmo</option>
                <option value="http://feeds.feedburner.com/thegadgetflow">Feedburner Thegadgetflow</option>
                <option value="https://www.pocket-lint.com/rss/cameras/all.xml">Pocket Lint Cameras</option>
                <option value="http://www.coolest-gadgets.com/feed/">Coolest Gadgets</option>
                <option value="https://www.reddit.com/r/gadgets/.rss">Reddit Gadgets</option>
                <option value="http://www.gadgetreview.com/reviews/feed">Gadget Review</option>
                <option value="http://gadgetstouse.com/feed">Gadgetstouse</option>
                <option value="https://www.instash.com/feed/">Instash</option>    
                <option value="http://feeds.feedburner.com/eset/blog">Feedburner Eset</option>    
                <option value="https://securingtomorrow.mcafee.com/feed/">Securingtomorrow</option>    
                <option value="https://krebsonsecurity.com/feed/">KrebsonSecurity</option>    
                <option value="https://www.schneier.com/blog/atom.xml">Schneier</option>    
                <option value="https://threatpost.com/feed/">Threatpost</option>    
                <option value="https://nakedsecurity.sophos.com/feed/">NakedSecurity</option>    
                <option value="https://blogs.quickheal.com/feed/">QuickHeal</option>    
                <option value="http://feeds.feedburner.com/GoogleOnlineSecurityBlog">Feedburner GoogleOnlineSecurityBlog</option>    
                 <option value="https://www.grahamcluley.com/feed/">Grahamcluley</option>    
                <option value="https://www.engadget.com/rss.xml">Engadget</option>    
                <option value="https://www.theverge.com/rss/index.xml">Theverge</option>    
                <option value="https://techcrunch.com/feed/">Techcrunch</option> 
                <option value="https://www.androidauthority.com/feed/">Android Authority</option>
                <option value="https://www.pcworld.com/index.rss">PC World</option>
                <option value="http://feeds.feedburner.com/thenextweb">Feedburner Thenextweb</option>
                <option value="https://www.cnet.com/rss/all/">Cnet</option>
                <option value="https://www.digitaltrends.com/feed/">Digital Trends</option>
                <option value="https://www.techradar.com/rss">Tech Radar</option>
                <option value="https://www.bgr.in/feed/">BGR</option>
                <option value="https://www.zdnet.com/news/rss.xml">Zdnet</option>
                <option value="https://gadgets.ndtv.com/rss/feeds">Gadgets</option>   
                <option value="https://www.techrepublic.com/rssfeeds/articles/?feedType=rssfeeds&sort=latest">Techrepublic</option> 
                <option value="https://www.techspot.com/backend.xml">Techspot</option> 
                <option value="https://www.anandtech.com/rss">Anandtech</option> 

                <option disabled >----------------------Health----------------------</option> 

                <option value="https://www.cancertherapyadvisor.com/pages/rss.aspx?sectionid=1316">Cancer Therapy Advisor</option> 
                <option value="http://feeds.feedburner.com/cancerresearchuk/SHhE">Feedburner Cancer Research UK</option> 
                <option value="http://blogs.cancer.org/drlen/feed/">blogs.cancer.org</option> 
                <option value="https://www.cancer.net/blog.xml">cancer.net</option> 
                <option value="https://www.oncolink.org/blogs/feed/">oncolink.org</option> 
                <option value="https://www.cancercouncil.com.au/feed/">cancercouncil.com.au</option> 
                <option value="https://triagecancer.org/blog/feed">triagecancer.org</option> 
                <option value="https://www.cancersupportcommunity.org/blog/feed">cancersupportcommunity.org</option> 
                <option value="http://www.cancerresearch.org/blog?rss=Cancer-Research-Institute-Blog">cancerresearch.org</option> 
                <option value="https://scopeblog.stanford.edu/tag/cancer-2/feed/">scopeblog.stanford.edu</option> 
                <option value="http://blog.thebreastcancersite.greatergood.com/feed/">thebreastcancersite.greatergood.com</option> 
                <option value="https://www.mensjournal.com/feed/">mensjournal.com</option> 
                <option value="http://fitness.reebok.com/api/blog/rss?locale=en-US">fitness.reebok.com</option> 
                <option value="http://blog.myfitnesspal.com/feed/">myfitnesspal.com</option>
             
                <option value="https://www.nataliejillfitness.com/feed/">nataliejillfitness.com</option> 
                <option value="https://www.runtastic.com/blog/en/feed/">runtastic.com</option> 
                <option value="http://feeds.feedburner.com/acefitness/fitnovatives">Feedburner Acefitness</option> 
                <option value="http://feeds.feedburner.com/anytimefitnessofficial">Feedburner Any Time Fitness Official</option> 
                <option value="https://www.afpafitness.com/blog/rss.xml">afpafitness.com</option> 
                <option value="http://www.bornfitness.com/feed/">bornfitness.com</option> 
                <option value="https://fitnessista.com/feed/">fitnessista.com</option> 
                <option value="https://totalgymdirect.com/total-gym-blog/feed">totalgymdirect.com</option> 
                <option value="https://www.getfitso.com/blog/feed/">Getfitso.com</option> 
                <option value="https://vasafitness.com/feed/">vasafitness.com</option> 
                <option value="http://healthworksfitness.com/feed/">healthworksfitness.com</option> 
                <option value="https://www.garmin.com/en-US/blog/feed/">garmin.com</option> 
                <option value="https://blog.underarmour.com/feed/">underarmour.com</option> 
                <option value="https://theyogawarrior.co/feed/">theyogawarrior.co</option> 
                <option value="https://www.jenreviews.com/sports-and-fitness/feed/">jenreviews.com</option> 
                <option value="https://www.drugs.com/feeds/medical_news.xml">drugs.com</option>                
                <option value="https://medlineplus.gov/groupfeeds/new.xml">medlineplus.gov</option> 
                <option value="https://www.fiercepharma.com/rss/pharma/xml">fiercepharma.com</option> 
                <option value="http://www.pharmatimes.com/rss/news_rss.rss">pharmatimes.com</option> 
                <option value="https://pharmaphorum.com/feed/">pharmaphorum.com</option> 
                <option value="https://www.pharmatutor.org/taxonomy/term/2758/all/feed">pharmatutor.org</option> 
                <option value="https://pharma.elsevier.com/feed/?x=1">pharma.elsevier.com</option> 
                <option value="http://www.pharmtech.com/sitefeed/3261/feed">pharmtech.com</option>  

                <option disabled >----------------------Sports----------------------</option> 

                <option value="http://www.espn.com/espn/rss/ncf/news">espn.com</option>  
                <option value="https://api.foxsports.com/v1/rss?partnerKey=zBaFxRyGKCfxBagJG9b8pqLyndmvo7UU">foxsports.com</option>  
                <option value="https://practical-golf.com/feed/">practical-golf.com</option>  
                <option value="https://www.reddit.com/r/golf/.rss">reddit.com</option>  
                <option value="http://www.espn.com/espn/rss/golf/news">espn.com golf</option>  
                <option value="https://www.golfchannel.com/rss/124552/feed.xml">golfchannel.com</option>  
                <option value="https://www.motorsport.com/rss/f1/news/">motorsport.com</option>  
                <option value="http://feeds.sport24.co.za/articles/Sport/OtherSport/rss">feeds.sport24.co.za</option>
                <option value="https://crickettimes.com/category/blog/feed/">crickettimes.com</option>               
                <option value="http://live-feeds.cricbuzz.com/CricbuzzFeed">live-feeds.cricbuzz.com</option>  
                <option value="http://www.espncricinfo.com/rss/content/story/feeds/0.xml">espncricinfo.com</option>  
                <option value="http://feeds.feedburner.com/tennisx">feeds.feedburner.com</option>  
                <option value="https://www.boxinginsider.com/feed/">boxinginsider.com</option>  
                <option value="http://www.insidehboboxing.com/inside?format=RSS">insidehboboxing.com</option>  
                <option value="http://www.boxingnewsonline.net/feed/">boxingnewsonline.com</option>  
                <option value="https://www.fia.com/rss/press-release">fia.com</option>  
                <option value="http://feeds.feedburner.com/tenniscom-news?format=xml">tenniscom-news</option>  
                <option value="https://www.prweb.com/rss2/sportsboxing.xml">prweb.com</option>  
                <option value="https://rss.cbssports.com/rss/headlines/">rss.cbssports.com</option>  

                <option disabled >----------------------Business----------------------</option> 

                <option value="http://feeds.feedburner.com/venturebeat/SZYF">feeds.feedburner.com</option>
                <option value="http://rss.cnn.com/rss/money_topstories.rss">rss.cnn.com</option>  
                <option value="http://www.cnbc.com/id/19746125/device/rss/rss.xml">cnbc.com</option>  
                <option value="http://fortune.com/feed/">fortune.com</option>  
                <option value="https://www.ft.com/?format=rss">ft.com</option>  
                <option value="https://finance.yahoo.com/news/rssindex">finance.yahoo.com</option>  
                <option value="https://www.investing.com/rss/news.rss">investing.com</option>  
                <option value="https://seekingalpha.com/market_currents.xml">seekingalpha.com</option>  
                <option value="http://feeds.feedburner.com/morningstar/glkd">feeds.feedburner.com</option>  
                <option value="https://www.musicbusinessworldwide.com/feed/">musicbusinessworldwide.com</option>  
                <option value="https://economictimes.indiatimes.com/rssfeedsdefault.cms">economictimes.indiatimes.com</option>  
                <option value="http://feeds.reuters.com/reuters/INtopNews">feeds.reuters.com</option>  
                <option value="https://www.livemint.com/rss/homepage">livemint.com</option>  
                <option value="https://www.financialexpress.com/feed/">financialexpress.com</option>  
                <option value="https://www.business-standard.com/rss/home_page_top_stories.rss">business-standard.com</option>  
                <option value="https://www.thehindubusinessline.com/?service=rss">thehindubusinessline.com</option>  
                <option value="https://www.businesstoday.in/rss/rssstory.jsp?sid=105">businesstoday.com</option>  
                <option value="http://www.globes.co.il/webservice/rss/rssfeeder.asmx/FeederNode?iID=1725">globes.co.il</option>  
                <option value="https://business.financialpost.com/feed/">business.financialpost.com</option>  

                <option disabled >----------------------Automotive----------------------</option> 

                <option value="https://www.motortrend.com/feed/">motortrend.com</option>  
                <option value="http://www.autoblog.com/rss.xml">autoblog.com</option>  
                <option value="https://feeds.feedburner.com/BmwBlog">feeds.feedburner.com BmwBlog</option>  
                <option value="http://feeds.feedburner.com/MotorAuthority2">feeds.feedburner.com MotorAuthority2</option>  
                <option value="https://www.automobilemag.com/feed/">automobilemag.com</option>  
                <option value="https://www.edmunds.com/feeds/rss/articles.xml">edmunds.com</option>  
                <option value="https://www.carscoops.com/">carscoops.com</option>  
                <option value="https://www.hemmings.com/blog/index.php/feed">hemmings.com</option>  
                <option value="http://paultan.org/feed/">paultan.com</option>  
                <option value="https://www.bimmerpost.com/feed/">bimmerpost.com</option>  
                <option value="https://www.cartalk.com/sites/default/files/rss/tomray.xml">cartalk.com</option>
                <option value="http://feeds.highgearmedia.com/?sites=GreenCarReports">highgearmedia.com</option>  
                <option value="http://feeds.feedburner.com/Speedhunters">feedburner.com</option>  
                <option value="https://www.cartoq.com/feed/">cartoq.com</option>  
                <option value="https://www.cars.com/news/rss.xml">cars.com</option>  
                <option value="http://blog.cargurus.com/feed">cargurus.com</option>  
                <option value="https://www.motor1.com/rss/news/all/">motor1.com</option>  
                <option value="https://www.supercars.net/blog/feed/">supercars.com</option>  
                <option value="https://www.youtube.com/feeds/videos.xml?user=supercarsoflondon">Super Cars Of London</option>  
                <option value="https://www.supercar-driver.com/feed/">supercar-driver.com</option> 

                <option disabled >----------------------Real Estate----------------------</option> 

                <option value="https://www.zillow.com/blog/feed/">zillow.com</option> 
                <option value="https://blog.kw.com/rss.xml">blog.kw.com</option> 
                <option value="https://www.biggerpockets.com/renewsblog/feed/">biggerpockets.com</option> 
                <option value="http://speakingofrealestate.blogs.realtor.org/feed/">speakingofrealestate.blogs.realtor.org</option> 
                <option value="http://feeds.feedburner.com/inmannews">feeds.feedburner.com</option> 
                <option value="https://www.reddit.com/r/RealEstate/.rss?format=xml">reddit.com</option> 
                <option value="https://www.redfin.com/blog/feed">redfin.com</option> 
                <option value="https://www.realestate.com.au/news/feed/">realestate.com</option> 
                <option value="http://infoservices.blogs.realtor.org/feed/">infoservices.com</option> 
                <option value="https://www.forbes.com/real-estate/feed/">forbes.com</option> 
                <option value="http://feeds.bizjournals.com/industry_21">feeds.bizjournals.com</option> 
                <option value="http://rss.cnn.com/rss/money_realestate.rss">rss.cnn.com</option> 
                <option value="http://www.latimes.com/business/realestate/rss2.0.xml">latimes.com</option> 
                <option value="http://www.cnbc.com/id/10000115/device/rss">cnbc.com</option> 
                <option value="https://www.seattletimes.com/business/real-estate/feed/">seattletimes.com</option> 
                <option value="http://feeds.bizjournals.com/industry_20">bizjournals.com</option> 
                <option value="https://www.century21.com/real-estate-blog/feed/">century21.com</option> 
                <option value="https://therealdeal.com/?feed=feed">therealdeal.com</option> 
                <option value="https://www.architecturaldigest.com/feed/rss">architecturaldigest.com</option> 
                <option value="https://feeds.feedburner.com/Archdaily">feedburner.com</option> 

                 <option disabled >----------------------Travel----------------------</option> 

                <option value="https://allafrica.com/tools/headlines/rdf/travel/headlines.rdf">feedburner.com</option> 
                <option value="https://www.getaway.co.za/feed/">getaway.co.za</option> 
                <option value="https://travel.jumia.com/blog/feed">travel.jumia.com</option> 
                <option value="http://blog.sa-venues.com/feed/">blog.sa-venues.com</option> 
                <option value="https://blog.rhinoafrica.com/feed/">rhinoafrica.com</option> 
                <option value="http://www.capetown.travel/feed/">capetown.travel</option> 
                <option value="https://theworldpursuit.com/blog/feed">theworldpursuit.com</option> 
                <option value="https://www.asiliaafrica.com/blog/feed/">asiliaafrica.com</option> 
                <option value="https://www.jenmansafaris.com/feed/">jenmansafaris.com</option> 
                <option value="http://www.expatcapetown.com/Cape-Town.xml">expatcapetown.com</option>
                <option value="https://news.google.com/news?cf=all&hl=en&pz=1&ned=us&q=africa+travel&output=rss">news.google.com</option> 
                <option value="https://safarijunkie.com/africa-travel/feed">safarijunkie.com</option> 
                <option value="http://www.travelsense.asia/blog/feed/">travelsense.com</option> 
                <option value="http://blog.homeaway.asia/feed/">homeaway.com</option> 
                <option value="https://www.thetravelninjas.com/feed/">thetravelninjas.com</option> 
                <option value="https://iamaileen.com/feed/">iamaileen.com</option> 
                <option value="https://www.explorient.com/feed/">explorient.com</option> 
                <option value="https://blog.straytravel.asia/feed/">straytravel.com</option> 
                <option value="https://homeiswhereyourbagis.com/en/feed/">homeiswhereyourbagis.com</option> 
                <option value="https://www.exotravel.com/blog/en/feed/">exotravel.com</option>     
                <option value="https://travelfreak.net/feed/">travelfreak.net</option>                 
                <option value="https://www.justonewayticket.com/rss/blog">justonewayticket.com</option>              
                <option value="https://www.jonesaroundtheworld.com/feed/">jonesaroundtheworld.com</option>           
                <option value="https://iamaileen.com/feed/">iamaileen.com</option>                 
                <option value="https://travelsofadam.com/blog/feed/">travelsofadam.com</option>                 
                <option value="https://lajollamom.com/blog/feed/">lajollamom.com</option>                 
                <option value="https://bucketlistjourney.net/feed/">bucketlistjourney.com</option>                 
                <option value="http://feeds.feedburner.com/ytravelblog">feedburner.com</option>                 
                <option value="https://www.goatsontheroad.com/feed/">goatsontheroad.com</option>                 
                <option value="https://theplanetd.com/feed/">theplanetd.com</option>                 
                <option value="https://www.nomadicmatt.com/travel-blog/feed/">nomadicmatt.com</option>
                <option value="http://www.worldofwanderlust.com/feed/">worldofwanderlust.com</option>
                <option value="https://twomonkeystravelgroup.com/feed/">twomonkeystravelgroup.com</option>
                <option value="http://www.fashionismo.com.br/feed/">fashionismo.com</option>
                <option value="http://braziljournal.com/rss">braziljournal.com</option>
                <option value="http://acaradorio.com/feed/">acaradorio.com</option>
                <option value="http://www.jornaldebrasilia.com.br/feed/">jornaldebrasilia.com</option>
                <option value="http://www.brasilwire.com/feed/">brasilwire.com</option>
                <option value="https://riotimesonline.com/feed/">riotimesonline.com</option>               

            </select>    
           </div>

      </td></tr>

  </table>
   
      </form>
 

   </div>

       
    <?php 
    
    $article_source_table = $wpdb->prefix . "article_source_tbl";
    $results = $wpdb->get_results("SELECT * FROM $article_source_table where source_type='RSS' ");
    
function objectToArray($d) {
        if (is_object($d)) {  
            $d = get_object_vars($d);
        }
  
        if (is_array($d)) {
           
            return array_map(__FUNCTION__, $d);
        }
        else {
            return $d;
        }
    }


$var1 = objectToArray($results);
?>

 <div class="postbox">
<?php

   for($i=0;$i<count($var1);$i++)
   {

 ?> 
   
     <form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">

    <table  style="overflow-x:auto;width: 100%;">
       <tr>
          <td width="25%">
                    
        <div class="inside">
            <strong>Select Article Source</strong>
            <br />               
            <input type="text"  name="feed_source_url" value="<?php echo $var1[$i]['source_name']; ?>" style="width:250px;">                 
           
        </div>
         </td>

        <td width="20%" >
            <div class="inside">
            <strong>Category</strong>
            <br />               
            <input type="text"  value="<?php echo $var1[$i]['source_category'];?>" name="news_rules_list_category" style="width:150px;" >             
            </div>
        </td>

   

          <td width="10%">
            <div class="inside">
             <strong>Max # Posts </strong>
            <br />               
             <input type="number" step="1" min="0" placeholder="# max" max="100" name="no_post" style="width:100px;" value="<?php echo $var1[$i]['no_post']; ?>" required="">       
            </div>
         </td>

         <td width="10%">
            <div class="inside">
             <strong>Post Status </strong>
            <br />               
            <select id="post_status" name="post_status" style="width:100px;">
              <option <?php if ($var1[$i]['post_status'] == "pending" ) echo 'selected' ; ?> value="pending">Moderate -&gt; Pending</option>
              <option <?php if ($var1[$i]['post_status'] == "draft" ) echo 'selected' ; ?> value="draft">Moderate -&gt; Draft</option>
              <option <?php if ($var1[$i]['post_status'] == "publish" ) echo 'selected' ; ?> value="publish" selected="">Published</option>
              <option <?php if ($var1[$i]['post_status'] == "private" ) echo 'selected' ; ?> value="private">Private</option>
              <option <?php if ($var1[$i]['post_status'] == "trash" ) echo 'selected' ; ?> value="trash">Trash</option>
            </select>    
            </div>
         </td>
           
         <td width="10%">
           <div class="inside">
             <strong>Item Type </strong>
            <br />               
            <select id="post_sel_type" name="post_sel_type" style="width:100px;">
             <option <?php if ($var1[$i]['post_sel_type'] == "post" ) echo 'selected' ; ?> value="post" selected="">post</option>
             <option <?php if ($var1[$i]['post_sel_type'] == "page" ) echo 'selected' ; ?> value="page">page</option>
             <option <?php if ($var1[$i]['post_sel_type'] == "attachment" ) echo 'selected' ; ?> value="attachment">attachment</option>
             <option <?php if ($var1[$i]['post_sel_type'] == "revision" ) echo 'selected' ; ?> value="revision">revision</option>
             <option <?php if ($var1[$i]['post_sel_type'] == "nav_menu_item" ) echo 'selected' ; ?> value="nav_menu_item">nav_menu_item</option>
             <option <?php if ($var1[$i]['post_sel_type'] == "custom_css" ) echo 'selected' ; ?> value="custom_css">custom_css</option>
             <option <?php if ($var1[$i]['post_sel_type'] == "customize_changeset" ) echo 'selected' ; ?> value="customize_changeset">customize_changeset</option>
             <option <?php if ($var1[$i]['post_sel_type'] == "oembed_cache" ) echo 'selected' ; ?> value="oembed_cache">oembed_cache</option>
             <option <?php if ($var1[$i]['post_sel_type'] == "user_request" ) echo 'selected' ; ?> value="user_request">user_request</option>
            </select>    
           </div>
        </td>
        
        <td width="10%">
            <div class="inside">
             <strong>Action </strong>
            <br />               
            <select id="post_action" name="post_action" style="width:100px;">
             <option <?php if ($var1[$i]['post_action'] == "run_this_rule_now" ) echo 'selected' ; ?> value="run_this_rule_now">Run This Rule Now</option>
              <option <?php if ($var1[$i]['post_action'] == "move_all_posts_to_trash" ) echo 'selected' ; ?> value="move_all_posts_to_trash">Move All Posts To Trash</option>
               <option <?php if ($var1[$i]['post_action'] == "duplicate_this_rule" ) echo 'selected' ; ?> value="duplicate_this_rule">Duplicate This Rule</option>
            <option <?php if ($var1[$i]['post_action'] == "permanently_delete_all_posts" ) echo 'selected' ; ?> value="permanently_delete_all_posts">Permanently Delete All Posts</option> 
              <option <?php if ($var1[$i]['post_action'] == "delete_this_rule" ) echo 'selected' ; ?> value="delete_this_rule">Delete This Rule</option>
              
            </select>    
            </div>
         </td> 

        <input type="hidden" name="postid" value="<?php echo $var1[$i]['id']; ?>">
       <td width="15%">
            <div class="inside">
             
            <br />               
            <input type="submit" name="slm_search_btn2" class="button button-primary" value="Save Article" />
            <br />            
            </div>
        </td>
        
      
     </tr>
   </table>
    </form>
      <?php 
        }
      ?>
</div>

            <?php
            include_once( 'rss_rule_class.php' ); //For rendering the license List Table
            $license_list = new WPLM_RSS_FEED();
            if (isset($_REQUEST['action'])) { 
                if (isset($_REQUEST['action'])) { 
                    $license_list->delete_licenses(strip_tags($_REQUEST['id']));
                }
            }
        
            $license_list->prepare_items_list();
          
            ?>    

     </div></div>
    </div>
    <?php
   
}


