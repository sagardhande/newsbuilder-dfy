<?php

function wp_help_support() {


    echo '<div class="wrap">';
    echo '<h2>Automatic Post Generator</h2>';
    echo '<div id="poststuff"><div id="post-body">';
    ?>

    <div class="postbox">
         
        <h3 class="hndle"><label for="title">DFY Content</label></h3>
        <div class="inside">
               <br /> 
          
            Please select Category to get DFY content. <br /> <br /> 

             <form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
                
                <select id="dfy_category" name="dfy_category" style="width:170px;">
                <option value="Technology">Technology</option>
                <option value="Health">Health</option>
                <option value="Gaming">Gaming</option>
                <option value="Business">Business</option>
                <option value="Automotive">Automotive</option>
                <option value="Real">Real Estate</option>
                <option value="Travel">Travel</option>
                <option value="Sports">Sports</option>
                <option value="Fashion">Fashion</option>
                <option value="Entertainment">Entertainment</option>
                <option value="Cancer">Cancer</option>
                <option value="Education">Education</option>
                <option value="Food and Drinks">Food and Drinks</option>
                <option value="Cryptocurrency">Cryptocurrency</option>
                <option value="Pets">Pets</option>
                <option value="Self Improvement">Self Improvement</option>
                <option value="Books">Books</option>
                <option value="Arts">Arts</option>
              
             </select>  

                <br />               
               <!--  <input name="allpost" type="checkbox" value="1"/> -->
                <br />
                <input type="submit" name="dfy_api_btn" class="button button-primary" value="Get Content" />
            </form>

          
        </div></div>

    <?php
    include_once( 'help-support-class.php' ); //For rendering the license List Table
    $license_list = new Register_Help_Support();
    $license_list->save_key2();
    ?>

    <?php
    echo '</div></div>';
    echo '</div>';
}

