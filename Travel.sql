(`id`, `source_name`, `auth_id`, `schedule_post`, `no_post`, `post_status`, `post_sel_type`, `date_created`, `source_type`, `source_category`) VALUES
(1, 'https://allafrica.com/tools/headlines/rdf/travel/headlines.rdf', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Africa'),
(3, 'https://www.getaway.co.za/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Africa'),
(4, 'https://travel.jumia.com/blog/feed', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Africa'),
(5, 'http://blog.sa-venues.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Africa'),
(6, 'https://blog.rhinoafrica.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Africa'),
(7, 'http://www.capetown.travel/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Africa'),
(8, 'https://theworldpursuit.com/blog/feed', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Africa'),
(9, 'https://www.asiliaafrica.com/blog/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Africa'),
(10, 'https://www.jenmansafaris.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Africa'),
(11, 'http://www.expatcapetown.com/Cape-Town.xml', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Africa'),
(12, 'https://news.google.com/news?cf=all&hl=en&pz=1&ned=us&q=africa+travel&output=rss', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Africa'),
(13, 'https://safarijunkie.com/africa-travel/feed', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Africa'),
(14, 'http://www.travelsense.asia/blog/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Asia'),
(15, 'http://blog.homeaway.asia/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Asia'),
(16, 'https://www.thetravelninjas.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Asia'),
(17, 'https://iamaileen.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Asia'),
(18, 'https://www.explorient.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Asia'),
(19, 'https://blog.straytravel.asia/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Asia'),
(20, 'https://homeiswhereyourbagis.com/en/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Asia'),
(21, 'https://www.exotravel.com/blog/en/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Asia'),
(22, 'http://www.global-gallivanting.com/category/south-east-asia/feed', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Asia'),
(23, 'https://etramping.com/destinations/asia/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Asia'),
(24, 'https://www.wildjunket.com/destinations/asia/feed/?x=1', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Asia'),
(25, 'https://blog.eurail.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Europe'),
(26, 'http://blog.traveleurope.com/en/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Europe'),
(27, 'http://www.toeuropeandbeyond.com/blog/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Europe'),
(28, 'https://www.getours.com/blog/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Europe'),
(29, 'https://rewildingeurope.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Europe'),
(30, 'https://euro-travels.com/feed/?x=1', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Europe'),
(31, 'https://blog.ricksteves.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Europe'),
(32, 'https://www.opodo.co.uk/blog/category/destinations/europe/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Europe'),
(33, 'http://feeds.feedburner.com/AdventurousKate', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Europe'),
(34, 'https://etramping.com/destinations/europe/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Europe'),
(35, 'https://travelsofadam.com/europe/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Europe'),
(36, 'https://www.wildjunket.com/destinations/europe/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Europe'),
(37, 'https://www.marbellafamilyfun.com/marbella.xml', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Europe'),
(38, 'http://travelgreecetraveleurope.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Europe'),
(39, 'https://www.vienna-unwrapped.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Europe'),
(40, 'http://www.germany.travel/en/news/feed.html', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Europe'),
(41, 'https://www.theguardian.com/travel/germany/rss', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Europe'),
(42, 'https://travelsofadam.com/europe/germany/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Europe'),
(43, 'http://www.1800flyeurope.com/RSS/1800Flyeurope.rss', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Europe'),
(44, 'http://whatson.ae/dubai/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Middle East'),
(45, 'https://visitabudhabi.ae/en/rss/events.aspx', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Middle East'),
(46, 'https://visitabudhabi.ae/en/rss/where.to.stay.aspx', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Middle East'),
(47, 'https://visitabudhabi.ae/en/rss/restaurants.aspx', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Middle East'),
(48, 'https://visitabudhabi.ae/en/rss/wellness.and.spa.aspx', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Music'),
(49, 'https://visitabudhabi.ae/en/rss/wellness.and.spa.aspx', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Middle East'),
(50, 'http://feeds.feedburner.com/traveldailynews/Europe-News', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Europe'),
(51, 'http://feeds.feedburner.com/traveldailynews/Asia-Pacific-News', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Africa'),
(52, 'http://feeds.feedburner.com/traveldailynews/Asia-Pacific-News', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Asia'),
(53, 'http://feeds.feedburner.com/traveldailynews/Middle-East-News', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Middle East'),
(54, 'http://feeds.feedburner.com/traveldailynews/Africa-News', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Africa'),
(55, 'https://www.tourism-review.com/weekly-news.rss', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Latest News'),
(56, 'https://www.eturbonews.com/feed', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Latest News'),
(57, 'https://www.tnooz.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Latest News'),
(58, 'http://unwtowire.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Latest News'),
(59, 'https://www.traveldailynews.com/rss', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Latest News'),
(60, 'http://www.travelweekly.co.uk/rss/news.xml', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Latest News'),
(61, 'https://skift.com/digital/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Latest News'),
(62, 'http://www.businesstravelnews.com/rss/business-travel-news', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Latest News'),
(63, 'http://tourismbreakingnews.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Latest News'),
(64, 'https://www.tourismtribe.com/news-room/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Latest News'),
(65, 'https://news.google.com/news?cf=all&hl=en&pz=1&ned=us&q=tourism&output=rss', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Latest News'),
(66, 'http://tourismeschool.com/tourism-marketing-blog/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Latest News'),
(67, 'https://blog.queensland.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Latest News'),
(68, 'http://www.uwishunu.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Latest News'),
(69, 'https://blog.sandiego.org/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Latest News'),
(70, 'https://www.indianholiday.com/blog/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Asia'),
(71, 'http://www.uwishunu.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Latest News'),
(72, 'https://blog.queensland.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Latest News'),
(73, 'http://www.holidayiq.com/blog/rss.xml', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Africa'),
(74, 'http://www.holidayiq.com/blog/rss.xml', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Asia'),
(75, 'http://www.natgeotraveller.in/india/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Asia'),
(76, 'http://www.transindiatravels.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Asia'),
(77, 'https://devilonwheels.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Asia'),
(78, 'http://keralablogexpress.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Asia'),
(79, 'http://keralablogexpress.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Uncategorized'),
(80, 'https://riotimesonline.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'N. America'),
(81, 'http://www.brasilwire.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'N. America'),
(82, 'http://www.jornaldebrasilia.com.br/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'N. America'),
(83, 'http://acaradorio.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'N. America'),
(84, 'http://braziljournal.com/rss', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'N. America'),
(85, 'http://feeds.feedburner.com/OBlogDoGoogleBrasil', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'N. America'),
(86, 'http://www.fashionismo.com.br/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'N. America'),
(87, 'https://twomonkeystravelgroup.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Travel Lifestyle'),
(88, 'http://www.worldofwanderlust.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Travel Lifestyle'),
(89, 'https://www.nomadicmatt.com/travel-blog/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Travel Lifestyle'),
(90, 'http://feeds.feedburner.com/Theblondeabroad/ScWo', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Travel Lifestyle'),
(91, 'https://theplanetd.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Travel Lifestyle'),
(92, 'https://www.goatsontheroad.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Travel Lifestyle'),
(93, 'http://feeds.feedburner.com/ytravelblog', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Travel Lifestyle'),
(94, 'https://bucketlistjourney.net/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Travel Lifestyle'),
(95, 'https://lajollamom.com/blog/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Travel Lifestyle'),
(96, 'https://travelsofadam.com/blog/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Travel Lifestyle'),
(97, 'https://iamaileen.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Travel Lifestyle'),
(98, 'https://www.jonesaroundtheworld.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Travel Lifestyle'),
(99, 'https://www.justonewayticket.com/rss/blog', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Travel Lifestyle'),
(100, 'https://travelfreak.net/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Travel Lifestyle'),
(101, 'http://manonthelam.com/feed/', NULL, '', 10, 'publish', 'post', '2018-10-25', 'RSS', 'Uncategorized');
